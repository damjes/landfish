:- module(plaintext,
	[ attach_db/1,
	  triple/3,
	  assert_triple/3,
	  retract_triple/3,
	  retractall_triple/3
	]).

:- use_module(library(persistency)).

:- persistent triple(subject:atom, verb:atom, object:atom).

attach_db(Config) :-
	member(file(File), Config),
	member(options(Opt), Config),
	db_attach(File, Opt).
