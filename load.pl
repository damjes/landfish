% first, load needed modules

:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(persistency)).

% then load a config

:- [config].

% next, load all parts of application

:- ['models/consult.pl'].
:- ['views/consult.pl'].
:- ['server/consult.pl'].
