watch_request(Request) :-
	format('Content-type: text/html~n~n', []),
	format('<html>~n', []),
	format('<head>~n'),
	format('<style>~n'),
	format('table, tr, td, th {border: solid 1px; border-collapse: collapse; padding: 5px}~n'),
	format('th {background-color: black; color: white}~n'),
	format('</style>~n'),
	format('</head>~n'),
	format('<table>~n'),
	format('<tr><th>Name</th><th>Value</th></tr>~n'),
	print_request(Request),
	format('~n</table>~n'),
	format('</html>~n', []).

print_request([]).
print_request([H|T]) :-
	H =.. [Name, Value],
	format('<tr><td>~w</td><td>~w</td></tr>~n', [Name, Value]),
	print_request(T).
