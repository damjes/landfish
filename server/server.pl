start_server :-
	server_config(ServerCfg),
	http_server(filter_localhost, ServerCfg).

% Only connections from localhost are allowed!
% First. I want create single user app, then, when app is functional, I want
% migrate from persistent to some real DB and add multiple users.
filter_localhost(R) :-
	member(peer(ip(127,0,0,1)), R), !,
	http_dispatch(R).
filter_localhost(R) :-
	http_reply_file('static/error/not_localhost.html', [], R).
