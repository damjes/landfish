What is landfish?
=================

Triple storage with friendly (InfoCentral-like) interface.

Why is it called landfish?
--------------------------

Some Polish media anounced message (not certain if it is true), that EU said,
that Helix Pomatia (Roman Snail) are fishes. They are called in English
"inland fish", but in Poland we have two translations. One means that snails are
fishes living in rivers (in land), but another, more common says, that snail
is fish and live on land (kinda oxymoron), so this piece of software is called
simply landfish.

This name is funny protest about destroying normal social relationship,
homopropaganda, feminism, leftism and many other European Union activities.
I strongly disagree to denying people's freedom, enormous taxation and common
propaganda. EU should be destroyed and its components should be independent
states.

I also wouldn't receive any money from EU for this project.

And here is reference: [http://www.rferl.org/content/In_France_Snails_Are_Now_Fish/1962107.html]

License
-------

M6PL

How can I run it?
=================

NOTA BENE: This app now only allows connections from localhost. Also, please
use localhost or 127.0.0.1 IP rather than you local IP address.

You need SWI-Prolog, then you "run" file 'run.pl'.

In Windows you just click on this file.
In GNU you issue: 'swipl -s run.pl'.

Now you can access [http://localhost:8000]

You can also edit config file 'config.pl', which is plain Prolog script.
